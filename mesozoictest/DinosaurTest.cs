using System;
using Xunit;

using Mesozoic;

namespace mesozoictest
{
    public class DinosaurTest
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.Equal("Louis", louis.GetName());
            Assert.Equal("Stegausaurus", louis.GetSpecie());
            Assert.Equal(12, louis.GetAge());
        }

        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Grrr", louis.Roar());
        }

        [Fact]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.SayHello());
            Assert.Equal("Je suis Nessie le Diplodocus, j'ai 11 ans.", nessie.SayHello());
        }


        [Fact]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Assert.Equal("Je suis Louis et je fais un câlin à Nessie.", louis.Hug(nessie));
            Assert.Equal("Je suis Nessie et je fais un câlin à Louis.", nessie.Hug(louis));
            Assert.Equal("Je suis Nessie et je ne peux pas me faire de câlin à moi-même :'(.", nessie.Hug(nessie));
        }
    }
}
