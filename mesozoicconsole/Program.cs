﻿using System;
using Mesozoic;

namespace mesozoicconsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello from Mesozoic!");
            Dinosaur louis, nessie;

            louis = new Dinosaur("Louis", "Stegausaurus", 12);
            nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Console.WriteLine("Présentation de nos dinosaures:");

            Console.WriteLine(louis.SayHello());
            Console.WriteLine(nessie.Roar());

            Console.WriteLine(louis.Hug(nessie));
            Console.WriteLine(nessie.SayHello());

            Console.WriteLine(louis.Hug(louis));
            Console.WriteLine(nessie.Hug(louis));
        }
    }
}
