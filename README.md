# 📣 Disclaimer

> Le repository git que vous êtes en train de visualiser n'a eu qu'une vocation pédagogique pour accompagner les modules enseignés en DUT 1A, 2A, LPRGI et 1A de cycle ingénieur.

> Son propos n'avait qu'une portée pédagogique et peut ne pas refléter l'état actuel ou encore les bonnes pratiques de la/les technologie(s) utilisée. 

## 🎯 Utilisation

Vous êtes libre de réutiliser librement le code présent dans ce repository. Prenez garde que le code est ici daté, non mis à jour et potentiellement ouvert aux failles et/ou bugs.

## 🦕 Crédit

[Samy Delahaye](https://delahayeyourself.info)


## 🪴 Descriptif du projet


### ![MesoizoicSolution](imgs/logo.png)

A solution for learning base of OOP and Csharp with .Net Core


## UML Diagramm

![UML diagramm](imgs/uml.png)

## How to ?

**Run unit tests ?**

```bash
cd mesozoictest
dotnet test
```

**Run console app ?**

```bash
cd mesozoicconsole
dotnet run
```

## Projects

MesozoicSolution is made of two projects

### MesozoicConsole

A dummy console application for manipulating some dinosaurs

### MesozoicTest

A dummy Xunit collection of unit tests for learning Unit test !

